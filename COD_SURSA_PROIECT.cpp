#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;
class film
{
public:
  film ()
  {
    titlu = "";
    an = 0;
    durata = 0;
    codfilm = 0;
  }
  film (string a, int b, int c, int d)
  {
    titlu = a;
    an = b;
    durata = c;
    codfilm = d;
  }
  string gettitlu ()
  {
    return titlu;
  }
  int getan ()
  {
    return an;
  }
  int getdurata ()
  {
    return durata;
  }
  int getcodfilm ()
  {
    return codfilm;
  }
  void settitlu (string e)
  {
    if (e.length () > 0)
      {
          titlu = e;
      }
    else
      {
          cout << "Nu ati introdus titlul" << endl;
      }
  }
  void setan (int f)
  {
    if (f > 1900 && f < 2022)
      {
          an = f;
      }
    else
      {
          cout << "Nu ati introdus anul corect" << endl;
      }
  }
  void setdurata (int g)
  {
    if (g > 59 && g < 240)
      {
          durata = g;
      }
    else
      {
          cout << "Nu ati introdus durata corecta" << endl;
      }
  }
  void setcodfilm (int h)
  {
    if (h > 1 && h < 30)
      {
          durata = h;
      }
    else
      {
          cout << "Nu ati introdus codul de film corect" << endl;
      }
  }
private:
  string titlu;
  int an;
  int durata;
  int codfilm;
};
class bilet
{
    public:
        bilet()
        {
            film = "";
            data = "";
            ora = 0;
            nrsala = 0;
            nrloc = 0;
        }
        bilet(string a, string b, int c, int d, int e)
        {
            film = a;
            data = b;
            ora = c;
            nrsala = d;
            nrloc = e;
        }
        string getfilm()
        {
            return film;
        }
        string getdata()
        {
            return data;
        }
        int getora()
        {
            return ora;
        }
        int getnrsala()
        {
            return nrsala;
        }
        int getnrloc()
        {
            return nrloc;
        }
        void setfilm(string f)
        {
            if(f.length()>0)
            {
                film = f;
            }
            else
            {
                cout<<"Nu ati introdus filmul"<<endl;
            }
        }
        void setdata(string g)
        {
            if(g.length() > 0)
            {
                data = g;
            }
            else
            {
                cout<<"Nu ati introdus data"<<endl;
            }
        }
        void setora(int h)
        {
            if (h > 11 && h < 24)
            {
                ora = h;
            }
            else
            {
                cout<<"Nu ati introdus ora corecta"<<endl;
            }
        }
        void setnrsala(int i)
        {
            if (i > 0 && i < 6)
            {
                nrsala = i;
            }
            else
            {
                cout<<"Nu ati introdus sala corecta"<<endl; 
            }
        }
         void setnrloc(int j)
        {
            if (j > 0 && j < 301)
            {
                nrloc = j;
            }
            else
            {
                cout<<"Nu ati introdus locul corect"<<endl; 
            }
        }
    private:
        string film;
        string data;
        int ora;
        int nrsala;
        int nrloc;
};
class sala
{
public:
  sala ()
  {
    nrsala = 0;
    capa = 0;
  }
  sala (int a, int b)
  {
    nrsala = a;
    capa = b;
  }
  int getnrsala ()
  {
    return nrsala;
  }
  int getcapa ()
  {
    return capa;
  }
  void setnrsala (int c)
  {
    if (c > 0 && c < 6)
      {
          nrsala = c;
      }
    else
      {
          cout << "Nu ati introdus corect numarul salii" << endl;
      }
  }
  void setcapa (int d)
  {
    if (d > 0 && d < 301)
      {
          capa = d;
      }
    else
      {
          cout << "Nu ati introdus corect capacitatea salii" << endl;
      }
  }
private:
  int nrsala;
  int capa;
};
class client
{
public:
  client ()
  {
    numeclt = "";
    CNP = "";
  }
  client (string a, string b)
  {
    numeclt = a;
    CNP = b;
  }
  string getnumeclt ()
  {
    return numeclt;
  }
  string getCNP ()
  {
    return CNP;
  }
  void setnumeclt (string c)
  {
    if (c.length () > 0)
      {
          numeclt = c;
      }
    else
      {
          cout << "Nu ati introdus numele clientului" << endl;
      }
  }
  void setCNP (string d)
  {
    if (d.length () > 0)
      {
          CNP = d;
      }
    else
      {
          cout << "Nu ati introdus CNP-ul clientului" << endl;
      }
  }
private:
  string numeclt;
  string CNP;
};
class program
{
public:
  program ()
  {
    data = nullptr;
    listafilme = nullptr;
  }
  program (char* a, int* b)
  {
    if (a != nullptr)
		{
			data = new char[8];
			for (int i = 0; i < 8; i++) 
			{
				data[i] = a[i];
			}
		}
		else
		{
			data = nullptr;
		}
    if (b != nullptr)
		{
			listafilme = new int[12];
			for (int i = 0; i < 12; i++) 
			{
				listafilme[i] = b[i];
			}
		}
		else
		{
			listafilme = nullptr;
		}
  }
  program (const program& aux3)
	{
		if (aux3.data != nullptr)
		{
			data = new char[8];
			strcpy_s(data, 8, aux3.data);
		}
		else
		{
			data = nullptr;
		}
		if (aux3.listafilme != nullptr)
		{
			listafilme = new int[12];
			for (int i = 0; i < 12; i++)
			{
				listafilme[i] = aux3[i];
			}
		}
		else
		{
			listafilme = nullptr;
		}
	}
	program& operator=(const program& aux4)
	{
		if (data != nullptr)
		{
			delete[] data;
		}
		if (listafilme != nullptr)
		{
			delete[] listafilme;
		}
		if (aux4.data != nullptr)
		{
			data = new char[8];
			strcpy_s(data, 8, aux4.data);
		}
		else
		{
			data = nullptr;
		}
		return *this;
	}
	program& operator=(const program& aux4)
	{
		if (data != nullptr)
		{
			delete[] data;
		}
		if (listafilme != nullptr)
		{
			delete[] listafilme;
		}
		if (aux4.data != nullptr)
		{
			data = new char[8];
			strcpy_s(data, 8, aux4.data);
		}
		else
		{
			data = nullptr;
		}
		if (aux4.listafilme != nullptr)
		{
			listafilme = new int[12];
			strcpy_s(listafilme, 12, aux4.listafilme);
		}
		else
		{
			listafilme = nullptr;
		}
		return *this;
	}
	~program()
	{
		if (data != nullptr)
		{
			delete[] data;
		}
		if (listafilme != nullptr)
		{
			delete[] listafilme;
		}
	}
  char* getdata()
	{
		if (data != nullptr)
		{
			char* aux1 = new char[8];
			for (int i = 0; i < 8; i++)
			{
				aux1[i] = data[i];
			}
			return aux1;
		}
		else
		{
			return nullptr;
		}
	}
	int* getlistafilme()
	{
		if (listafilme != nullptr)
		{
			int* aux2 = new int[12];
			for (int i = 0; i < 12; i++)
			{
				aux2[i] = listafilme[i];
			}
			return aux2;
		}
		else
		{
			return nullptr;
		}
	}
  void setdata(char* c)
	{
		if (data != nullptr)
		{
			delete[] data;
		}
		if (c != nullptr)
		{
			data = new char[8];
			for (int i = 0; i < 8; i++)
			{
				data = &c[i];
			}
		}
		else
		{
			data = nullptr;
		}
	}
	void setlistafilme(int* d)
	{
		if (listafilme != nullptr)
		{
			delete[] listafilme;
		}
		if (d != nullptr)
		{
			listafilme = new int[12];
			for (int i = 0; i < 12; i++)
			{
				listafilme = &d[i];
			}
		}
		else
		{
			listafilme = nullptr;
		}
	}
private:
  char* data;
  int* listafilme;
};
int main ()
{
  string aux1;
  int aux2;
  film f1;
  cout << "introduceti numele filmului: ";
  cin >> aux1;
  f1.settitlu (aux1);
  cout << f1.gettitlu () << endl;
  cout << "introduceti anul filmului: ";
  cin >> aux2;
  f1.setan (aux2);
  cout << f1.getan () << endl;
  cout << "introduceti durata filmului: ";
  cin >> aux2;
  f1.setdurata (aux2);
  cout << f1.getdurata () << endl;
  return 0;
}
